//
//  RocksDB.h
//  RocksDB
//
//  Created by Aziz on 2015-04-28.
//  Copyright (c) 2015 PragmaOnce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "rocksdb/db.h"

@interface RocksDB : NSObject
- (void)test;
@end
