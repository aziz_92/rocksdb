//
//  RocksDB.m
//  RocksDB
//
//  Created by Aziz on 2015-04-28.
//  Copyright (c) 2015 PragmaOnce. All rights reserved.
//

#import "RocksDB.h"

using namespace rocksdb;
std::string kDBPath = "/tmp/rocksdb_simple_example";


@implementation RocksDB

- (void)test {
    DB* db;
    Options options;
    // Optimize RocksDB. This is the easiest way to get RocksDB to perform well
    //options.IncreaseParallelism();
    //options.OptimizeLevelStyleCompaction();
    // create the DB if it's not already present
    options.create_if_missing = true;
    
    // open DB
    Status s = DB::Open(options, kDBPath, &db);
    assert(s.ok());
    
    // Put key-value
    s = db->Put(WriteOptions(), "key1", "value");
    assert(s.ok());
    std::string value;
    // get value
    s = db->Get(ReadOptions(), "key1", &value);
    assert(s.ok());
    assert(value == "value");
    
    // atomically apply a set of updates
    {
        WriteBatch batch;
        batch.Delete("key1");
        batch.Put("key2", value);
        s = db->Write(WriteOptions(), &batch);
    }
    
    s = db->Get(ReadOptions(), "key1", &value);
    assert(s.IsNotFound());
    
    db->Get(ReadOptions(), "key2", &value);
    assert(value == "value");
    
    delete db;
}

@end
